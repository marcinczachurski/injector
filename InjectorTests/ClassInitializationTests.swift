//
//  ComponentTests.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit
import XCTest

class ClassInitializationTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_object_factory_must_return_correct_instance_for_class_initialization_with_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var returnedCat:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat.name() == "cat", "Object factory returned incorrect object for class initialization with lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_for_class_initialization_without_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Dog).forSameClass().withOneInstance().toGlobalContext()
        
        // Act.
        var returnedDog:Dog = ObjectFactory.getInstance(Dog)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedDog.name() == "dog", "Object factory returned incorrect object for class initialization without lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_for_class_initialization_for_protocol_with_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withOneInstance().toContainer()
        
        // Act.
        var returnedSnake:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedSnake.name() == "snake", "Object factory returned incorrect object for class initialization for protocol and with lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_for_class_initialization_for_protocol_without_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withOneInstance().toGlobalContext()
        
        // Act.
        var returnedSnake:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedSnake.name() == "snake", "Object factory returned incorrect object for class initialization for protocol and without lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_for_class_initialization_with_class_name_and_with_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forClass(Cat).withOneInstance().toContainer()
        
        // Act.
        var returnedCat:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat.name() == "cat", "Object factory returned incorrect object for class initialization with lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_for_class_initialization_with_class_name_and_without_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Dog).forClass(Dog).withOneInstance().toGlobalContext()
        
        // Act.
        var returnedDog:Dog = ObjectFactory.getInstance(Dog)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedDog.name() == "dog", "Object factory returned incorrect object for class initialization without lazy loading")
    }
    
    func test_object_factory_must_return_correct_instance_from_global_context()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toGlobalContext()
        
        // Act.
        var returnedCat:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat.name() == "cat", "Object factory returned incorrect object from global context")
    }
    
    func test_object_factory_must_return_correct_instance_from_custom_context()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContext("different")
        
        // Act.
        var returnedCat:Cat = ObjectFactory.fromContext("different").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat.name() == "cat", "Object factory returned incorrect object for class initialization with lazy loading")
    }
    
    func test_object_factory_must_return_diffrent_instances_from_diffrent_contexts()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContext("cont1")
        Container.addComponent(Cat).forSameClass().withOneInstance().toContext("cont2")
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.fromContext("cont1").getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.fromContext("cont2").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object from diffrent contexts")
    }
    
    func test_object_factory_must_return_diffrent_instances_from_diffrent_contexts_from_container()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.fromContext("cont1").getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.fromContext("cont2").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object from diffrent contexts")
    }

    func test_object_factory_must_return_diffrent_instances_for_per_call_initialization()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withInstancePerCall()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object from diffrent contexts")
    }
}
