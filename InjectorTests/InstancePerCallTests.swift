//
//  InstancePerCallTests.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit
import XCTest

class InstancePerCallTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_object_factory_must_return_different_instance_per_call_objects()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withInstancePerCall()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object for instance per call class initialization")
    }
    
    func test_object_factory_must_return_same_instance_per_call_objects()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 === returnedCat2, "Object factory returned this diffrent object for one instance class initialization")
    }
    
    func test_object_factory_must_return_different_instance_per_call_objects_for_class_name()
    {
        // Arrange.
        Container.addComponent(Cat).forClass(Cat).withInstancePerCall()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object for instance per call class initialization")
    }
    
    func test_object_factory_must_return_same_instance_per_call_objects_for_class_name()
    {
        // Arrange.
        Container.addComponent(Cat).forClass(Cat).withOneInstance().toContainer()
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 === returnedCat2, "Object factory returned this diffrent object for one instance class initialization")
    }
    
    func test_object_factory_must_return_different_instance_per_call_objects_for_protocol()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withInstancePerCall()
        
        // Act.
        var returnedSnake1:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        var returnedSnake2:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedSnake1 !== returnedSnake2, "Object factory returned this same object for instance per call class initialization for protocol")
    }
    
    func test_object_factory_must_return_same_instance_per_call_objects_for_protocol()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withOneInstance().toContainer()
        
        // Act.
        var returnedSnake1:Snake  = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        var returnedSnake2:Snake  = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedSnake1 === returnedSnake2, "Object factory returned this diffrent object for one instance class initialization for protocol")
    }

}
