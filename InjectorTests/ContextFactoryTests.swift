//
//  ContextFactoryTests.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit
import XCTest

class ContextFactoryTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_object_factory_must_return_same_objects_from_same_context()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var cat1:Cat = ObjectFactory.fromContext("AnimalContext").getInstance(Cat)
        var cat2:Cat = ObjectFactory.fromContext("AnimalContext").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(cat1 === cat2, "Object factory return diffrent objects from same context")
    }
    
    func test_object_factory_must_return_diffrent_objects_from_diffrent_contexts()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var cat1:Cat = ObjectFactory.fromContext("ContextA").getInstance(Cat)
        var cat2:Cat = ObjectFactory.fromContext("ContextB").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(cat1 !== cat2, "Object factory return same objects from diffrent context")
    }
}
