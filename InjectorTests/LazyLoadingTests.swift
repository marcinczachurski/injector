//
//  LazyLoadingTests.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit
import XCTest

class LazyLoadingTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_object_factory_must_have_not_initialized_component_for_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 0, "Object factory initialize object too fast with enabled lazy loading")
    }
    
    func test_object_factory_must_have_initialized_component_after_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()
        
        // Act.
        var returnedDog:Cat = ObjectFactory.getInstance(Cat)
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 1, "Object factory not initialize object after lazy loading")
    }
    
    func test_object_factory_must_have_not_initialized_component_for_class_name_and_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forClass(Cat).withOneInstance().toContainer()
        
        // Act.
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 0, "Object factory initialize object too fast with enabled lazy loading")
    }
    
    func test_object_factory_must_have_initialized_component_for_class_name_after_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Cat).forClass(Cat).withOneInstance().toContainer()
        
        // Act.
        var returnedDog:Cat = ObjectFactory.getInstance(Cat)
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 1, "Object factory not initialize object after lazy loading")
    }
    
    func test_object_factory_must_have_not_initialized_component_for_protocol_and_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withOneInstance().toContainer()
        
        // Act.
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 0, "Object factory initialize object too fast for protocol with enabled lazy loading")
    }
    
    func test_object_factory_must_have_initialized_component_for_protocol_after_lazy_loading()
    {
        // Arrange.
        Container.addComponent(Snake).forProtocol(AnimalProtocol).withOneInstance().toContainer()
        
        // Act.
        var returnedSnake:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        var amountOfInitialized = ObjectFactory.amountOfCreatedObjects()
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(amountOfInitialized == 1, "Object factory not initialize object for protocol after lazy loading")
    }
}
