//
//  InjectorTests.swift
//  InjectorTests
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit
import XCTest

class InstanceInitializationTests: XCTestCase {
    
    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_object_factory_must_return_correct_instance_for_instance_initialization()
    {
        // Arrange.
        var expectedDog = Dog()
        Container.addInstance(expectedDog).forSameClass().toGlobalContext()
        
        // Act.
        var returnedDog:Dog = ObjectFactory.getInstance(Dog)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedDog.name() == "dog", "Object factory returned incorrect object for instance initialization")
        XCTAssert(returnedDog === expectedDog, "Object factory returned different object for instance initialization")
    }
    
    func test_object_factory_must_return_correct_instance_for_instance_initialization_for_class_name()
    {
        // Arrange.
        var expectedDog = Dog()
        Container.addInstance(expectedDog).forClass(Dog).toGlobalContext()
        
        // Act.
        var returnedDog:Dog = ObjectFactory.getInstance(Dog)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedDog.name() == "dog", "Object factory returned incorrect object for instance initialization")
        XCTAssert(returnedDog === expectedDog, "Object factory returned different object for instance initialization")
    }
    
    func test_object_factory_must_return_correct_instance_for_instance_initialization_for_protocol()
    {
        // Arrange.
        var expectedSnake = Snake()
        Container.addInstance(expectedSnake).forProtocol(AnimalProtocol).toGlobalContext()
        
        // Act.
        var returnedSnake:Snake = ObjectFactory.getInstanceForProtocol(AnimalProtocol)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedSnake.name() == "snake", "Object factory returned incorrect object for instance initialization for protocol")
        XCTAssert(returnedSnake === expectedSnake, "Object factory returned different object for instance initialization for protocol")
    }
    
    func test_object_factory_must_return_correct_instance_for_instance_initialization_from_global_context()
    {
        // Arrange.
        var expectedCat = Cat()
        Container.addInstance(expectedCat).forSameClass().toGlobalContext()
        
        // Act.
        var returnedCat:Cat = ObjectFactory.getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(expectedCat === returnedCat, "Object factory returned incorrect object from global context")
    }
    
    func test_object_factory_must_return_correct_instance_for_instance_initialization_from_custom_context()
    {
        // Arrange.
        var expectedCat = Cat()
        Container.addInstance(expectedCat).forSameClass().toContext("different")
        
        // Act.
        var returnedCat:Cat = ObjectFactory.fromContext("different").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(expectedCat === returnedCat, "Object factory returned incorrect object from global context")
    }
    
    func test_object_factory_must_return_correct_diffrent_instances_from_diffrent_contexts()
    {
        // Arrange.
        Container.addInstance(Cat()).forSameClass().toContext("cont1")
        Container.addInstance(Cat()).forSameClass().toContext("cont2")
        
        // Act.
        var returnedCat1:Cat = ObjectFactory.fromContext("cont1").getInstance(Cat)
        var returnedCat2:Cat = ObjectFactory.fromContext("cont2").getInstance(Cat)
        Container.clear()
        ObjectFactory.clear()
        
        // Assert.
        XCTAssert(returnedCat1 !== returnedCat2, "Object factory returned this same object from diffrent contexts")
    }
}
