//
//  Animal.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

@objc protocol AnimalProtocol
{
   func name() -> String
}
