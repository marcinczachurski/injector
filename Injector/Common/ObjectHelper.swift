//
//  ObjectHelper.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

class ObjectHelper {
   
    class func getClassName(aClass: AnyClass!) -> String
    {
        return NSStringFromClass(aClass)
    }
    
    class func getProtocolName(proto: Protocol!) -> String!
    {
        return NSStringFromProtocol(proto)
    }

    class func getObjectForComponent(component:Component, contextName:String) -> AnyObject!
    {
        var createdObject:AnyObject! = NSClassFromString(component.componentClassName).alloc()
        
        if(createdObject.conformsToProtocol(Contextable))
        {
            var contextableObject = createdObject as Contextable
            contextableObject.setContextName(contextName)
        }
        
        return createdObject
    }
}
