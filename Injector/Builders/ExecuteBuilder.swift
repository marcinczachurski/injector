//
//  ExecuteBuilder.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class ExecuteBuilder {
   
    private var _component:Component
    private var _object:AnyObject
        
    init(component: Component, object:AnyObject)
    {
        _component = component
        _object = object
    }
    
    public func toGlobalContext()
    {
        ObjectFactory.addObjectToGlobalContext(_component, newObject: _object)
        Container.addComponentToContainer(_component)
    }
    
    public func toContext(contextName:String)
    {
        ObjectFactory.addObjectToContext(_component, newObject: _object, contextName: contextName)
        Container.addComponentToContainer(_component)
    }
}
