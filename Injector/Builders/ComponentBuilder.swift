//
//  ComponentBuilder.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class ComponentBuilder {
    
    private var _component:Component
    
    init(component: Component)
    {
        _component = component
    }
    
    public func forSameClass() -> MultipleBuilder
    {
        return MultipleBuilder(component: _component)
    }
    
    public func forClass(className: AnyClass) -> MultipleBuilder
    {
        _component.componentKey = ObjectHelper.getClassName(className)
        return MultipleBuilder(component: _component)
    }
    
    public func forProtocol(implementingProtocol:Protocol) -> MultipleBuilder
    {
        _component.componentKey = ObjectHelper.getProtocolName(implementingProtocol)
        return MultipleBuilder(component: _component)
    }
}
