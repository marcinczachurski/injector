//
//  LazyLoadingBuilder.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class LazyLoadingBuilder {
   
    private var _component:Component
    
    init(component: Component)
    {
        _component = component
    }
    
    public func toContext(contextName:String)
    {
        createAndAddObjectToContext(contextName)
    }
    
    public func toGlobalContext()
    {
        createAndAddObjectToContext(ObjectFactory.getGlobalContextName())
    }
    
    public func toContainer()
    {
        _component.componentLazyLoading = true
        Container.addComponentToContainer(_component)
    }
    
    private func createAndAddObjectToContext(contextName:String)
    {
        _component.componentLazyLoading = false
        var newObject:AnyObject! =  ObjectHelper.getObjectForComponent(_component, contextName: contextName)
        ObjectFactory.addObjectToContext(_component, newObject: newObject, contextName: contextName)
        Container.addComponentToContainer(_component)
    }
}
