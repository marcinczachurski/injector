//
//  MultipleBuilder.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class MultipleBuilder {
   
    private var _component:Component
    
    init(component: Component)
    {
        _component = component
    }
    
    public func withOneInstance() -> LazyLoadingBuilder
    {
        _component.componentInstancePerCall = false
        return LazyLoadingBuilder(component: _component)
    }
    
    public func withInstancePerCall()
    {
        _component.componentInstancePerCall = true
        Container.addComponentToContainer(_component)
    }
}
