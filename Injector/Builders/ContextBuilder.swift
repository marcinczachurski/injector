//
//  ContextBuilder.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class ContextBuilder
{
    private var _contextName:String
    
    init(contextName: String)
    {
        _contextName = contextName
    }
    
    public func getInstance<T:AnyObject>(classDefinition:AnyClass) -> T
    {
        var className = ObjectHelper.getClassName(classDefinition)
        var component:Component? = Container.getComponent(className)
        var objectToReturn: AnyObject? = getObjectToReturn(component!)
        return objectToReturn as T
    }
        
    /*! Get object by protocol name. */
    public func getInstanceForProtocol<T:AnyObject>(protocolDefinition:Protocol) -> T
    {
        var protocolName = ObjectHelper.getProtocolName(protocolDefinition)
        var component:Component? = Container.getComponent(protocolName)
        var objectToReturn: AnyObject? = getObjectToReturn(component!)
        return objectToReturn as T
    }
    
    private func getObjectToReturn(component:Component) -> AnyObject?
    {
        // Object created per call.
        if(component.componentInstancePerCall)
        {
            return ObjectHelper.getObjectForComponent(component, contextName:_contextName)
        }
        
        return ObjectFactory.getOrCreateObjectFromContext(component, contextName: _contextName)
    }
}
