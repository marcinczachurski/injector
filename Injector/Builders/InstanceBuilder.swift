//
//  InstanceBuilder.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class InstanceBuilder {
   
    private var _component:Component
    private var _object:AnyObject
    
    init(component: Component, object:AnyObject)
    {
        _component = component
        _object = object
    }
    
    public func forSameClass() -> ExecuteBuilder
    {
        return ExecuteBuilder(component: _component, object: _object)
    }
    
    public func forClass(className: AnyClass) -> ExecuteBuilder
    {
        _component.componentKey = ObjectHelper.getClassName(className)
        return ExecuteBuilder(component: _component, object: _object)
    }
    
    public func forProtocol(implementingProtocol:Protocol) -> ExecuteBuilder
    {
        _component.componentKey = ObjectHelper.getProtocolName(implementingProtocol)
        return ExecuteBuilder(component: _component, object: _object)
    }
}
