//
//  Contextable.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

@objc protocol Contextable
{
    func setContextName(name:String)
}
