//
//  Component.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 26.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class Component
{   
    public var componentKey:String
    public var componentClassName:String
    public var componentInstancePerCall:Bool
    public var componentLazyLoading: Bool
        
    init(className:String)
    {
        componentKey = ""
        componentClassName = className
        componentInstancePerCall = false
        componentLazyLoading = false
    }
    
    init(key:String, className:String)
    {
        componentKey = key
        componentClassName = className
        componentInstancePerCall = false
        componentLazyLoading = false
    }
}
