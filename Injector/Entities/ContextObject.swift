//
//  ContextObject.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

class ContextObject
{
    var contextComponent:Component
    var contextObject:AnyObject
    
    init(component:Component, object:AnyObject)
    {
        contextComponent = component
        contextObject = object
    }
}
