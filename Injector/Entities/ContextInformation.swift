//
//  Context.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

public class ContextInformation
{
    private var objectsInContext = [String: ContextObject]()
    
    func getContextObjectForComponent(component:Component) -> ContextObject?
    {
        return objectsInContext[component.componentKey]
    }
    
    func addContextObjectForComponent(component:Component, newObject: AnyObject)
    {
        objectsInContext[component.componentKey] = ContextObject(component: component, object: newObject)
    }
    
    func amountOfObjects() -> Int
    {
        return objectsInContext.count
    }
    
    func printContextInformation()
    {
        var index = 1
        for (key, object) in objectsInContext
        {
            println("\(index) - [Key: \(key)] : [ClassName: \(object.contextComponent.componentClassName)] : [InstancePerCall: \(object.contextComponent.componentInstancePerCall)]")
            index++
        }
    }
}
