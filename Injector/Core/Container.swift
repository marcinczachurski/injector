//
//  ObjectContainer.swift
//  Injector
//
//  Created by Marcin Czachurski on 27.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import UIKit

var _container = [String: Component]()

public class Container: NSObject
{
    /*! Add object to container. */
    public class func addInstance<T:AnyObject>(object:T) -> InstanceBuilder
    {
        var className = ObjectHelper.getClassName(T.self)
        var component = Component(key:className, className: className)
        var instanceBuilder = InstanceBuilder(component: component, object: object)
        return instanceBuilder
    }
    
    /*! Add class to container. Object will be generated automatically. */
    public class func addComponent(classDefinition:AnyClass) -> ComponentBuilder
    {
        var className = ObjectHelper.getClassName(classDefinition)
        var component = Component(key:className, className: className)
        var componentBuilder = ComponentBuilder(component: component)
        return componentBuilder
    }
    
    public class func clear()
    {
        _container = [String: Component]()
    }
    
    public class func amountOfAllComponents() -> Int
    {
        return _container.count
    }
    
    /*! Print information about all components in container. */
    public class func printAllComponents()
    {
        var index = 1
        for (key, object) in _container
        {
            println("\(index) - [Key: \(key)] : [ClassName: \(object.componentClassName)] : [InstancePerCall: \(object.componentInstancePerCall)]")
            index++
        }
    }
    
    internal class func addComponentToContainer(component:Component)
    {
        _container[component.componentKey] = component
    }
    
    internal class func getComponent(componentKey: String) -> Component
    {
        return _container[componentKey]!
    }
}
