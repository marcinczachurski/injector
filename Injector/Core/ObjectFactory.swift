//
//  ObjectFactory.swift
//  SwiftTest
//
//  Created by Marcin Czachurski on 24.07.2014.
//  Copyright (c) 2014 Marcin Czachurski. All rights reserved.
//

import Foundation

var _contexts = [String: ContextInformation]()
let _globalContextName:String = "**GlobalContext**"

public class ObjectFactory
{    
    /*! Get object by class name. */
    public class func getInstance<T:AnyObject>(classDefinition:AnyClass) -> T
    {
        return fromContext(_globalContextName).getInstance(classDefinition)
    }
    
    /*! Get object by protocol name. */
    public class func getInstanceForProtocol<T:AnyObject>(protocolDefinition:Protocol) -> T
    {
        return fromContext(_globalContextName).getInstanceForProtocol(protocolDefinition)
    }
    
    public class func fromContext(contextName: String) -> ContextBuilder
    {
        return ContextBuilder(contextName: contextName)
    }

    public class func clear(contextName: String)
    {
        var index = _contexts.indexForKey(contextName)
        if(index != nil)
        {
            _contexts.removeAtIndex(index!)
        }
    }
    
    public class func clear()
    {
        _contexts = [String: ContextInformation]()
    }
    
    public class func amountOfCreatedObjects() -> Int
    {
        var amount = 0
        for (contexName, contextInformation) in _contexts
        {
            amount += contextInformation.amountOfObjects()
        }
        return amount
    }
    
    public class func amountOfCreatedObjects(contextName:String) -> Int
    {
        var contextInformation = _contexts[contextName];
        if(contextInformation != nil)
        {
            return contextInformation!.amountOfObjects()
        }
        return 0;
    }
    
    public class func printAllContext()
    {
        var index = 1
        for (contextName, contextInfrmation) in _contexts
        {
            println("\(index) - [ContextName: \(contextName)] -------------------------------")
            contextInfrmation.printContextInformation()
            index++
        }
    }
    
    public class func getGlobalContextName() -> String
    {
        return _globalContextName
    }
    
    internal class func getOrCreateObjectFromContext(component:Component, contextName: String) -> AnyObject?
    {
        var contextInformation = _contexts[contextName]
        
        if(contextInformation == nil)
        {
            contextInformation = ContextInformation()
            _contexts[contextName] = contextInformation
        }
        
        var contextObject = contextInformation?.getContextObjectForComponent(component)
        if(contextObject != nil)
        {
            return contextObject?.contextObject
        }
        else
        {
            var newObject: AnyObject! = ObjectHelper.getObjectForComponent(component, contextName:contextName)
            contextInformation?.addContextObjectForComponent(component, newObject: newObject)
            return newObject
        }
    }
    
    internal class func addObjectToGlobalContext(component:Component, newObject:AnyObject)
    {
        addObjectToContext(component, newObject: newObject, contextName: _globalContextName)
    }
    
    internal class func addObjectToContext(component:Component, newObject:AnyObject, contextName:String)
    {
        var contextInformation = _contexts[contextName]
        
        if(contextInformation == nil)
        {
            contextInformation = ContextInformation()
            _contexts[contextName] = contextInformation
        }
        
        var contextObject = contextInformation?.getContextObjectForComponent(component)
        if(contextObject == nil)
        {
            contextInformation?.addContextObjectForComponent(component, newObject: newObject)
        }
    }
}