# Injector
This is a short introduction to Injector…

## What is Injector
Injector is simple framework which can help you to inject dependency to your objects. After initialization you can ask Injector for instances of specific classes. You can ask by class or protocol name. If Injector doesn't have any instance of this class it will create one.

## Container and ObjectFactory
Injector has two main public classes which are:

1. Container - it’s a class for all our class definitions. Class definition is saved in object named: Component;
2. ObjectFactory - it's a class for all instances of our classes. Object factory operates on internal boxes: contexts. It is one global context, user can also create own contexts. All of objects in contexts are connected with components in container.

![Container and ObjectFactory](https://copy.com/Qmj2g9mtfK9E1ccg "Container and ObjectFactory")

## Initialization
Here is a call graph of all possible ways of initializations.

![Container initialization](https://copy.com/7nEjXg6EifAKRHXQ "Container initialization")

Generally we have two ways of initialization:

1. add instance - this method creates instance of class (or we have to specify one) and always puts it to context box. Injector will always return this instance of object (if we ask for this same context). Initialization can be finished in two ways:
    - `toGlobalContext()` - object is saved in global context in ObjectFactory;
    - `toContext(contextName:String)` - object is saved in user defined context;
2. add component - this is only information about class. We have to decide when Injector will create instance of class. For object ceratek per call and when we only add component to cotainer, Injector will use lazy loading (so object will be created when we ask for it). When we add component to context, Injector will create object during initialization. Thus initialization can be finished in few ways:
    - `toGlobalContext()` - object is saved in global context in ObjectFactory;
    - `toContext(contextName:String)` - object is saved in user defined context;
    - `withObjectPerCall()` - Injector will always return new instance of class, so only component in a container is saved;
    - `toContainer()` - Injector will add component to container. Object is created once (per context) when user asks for it (lazy loading).

## Examples
### Object initialization
This kind of initialization adds object to context (global or user defined) and creates component in a container. This requires real object in method parameters.

#### For same class
Adding object to container:

	Container.addInstance(Dog()).forSameClass().toGlobalContext()

Asking for object:

	var returnedDog:Dog = ObjectFactory.getInstance(Dog)

#### For specyfic class

Adding object to container:

	Container.addInstance(Dog()).forClass(Dog).toGlobalContext()

Asking for object:

	var returnedDog:Dog = ObjectFactory.getInstance(Dog)

#### For protocol

Protocol definition:

	@objc protocol AnimalProtocol
	{
		func name() -> String
	}

Adding object to container:

	Container.addInstance(Dog()).forProtocol(AnimalProtocol).toGlobalContext()

Asking for object:

	var returnedDog:Dog = ObjectFactory.getInstanceForProtocol(AnimalProtocol)

### Component initialization
This kind of initialization adds component to a container. Then it is possible to create objects (during initialization or during request for object).

#### For same class
Adding component to container:

	Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()

Asking for object:

	var returnedCat:Cat = ObjectFactory.getInstance(Cat)

#### For specyfic class

Adding component to container:

	Container.addComponent(Cat).forClass(Cat).withOneInstance().toContainer()

Asking for object:

	var returnedCat:Cat = ObjectFactory.getInstance(Cat)

#### For protocol

Protocol definition:

	@objc protocol AnimalProtocol
	{
		func name() -> String
	}

Adding component to container:

	Container.addComponent(Cat).forProtocol(AnimalProtocol).withOneInstance().toContainer()

Asking for object:

	var returnedCat:Cat = ObjectFactory.getInstanceForProtocol(AnimalProtocol)

### Contexts
You can create objects in contexts. In different context you can have diffrent object of this same component.

Adding component to container:

	Container.addComponent(Cat).forSameClass().withOneInstance().toContainer()

Asking for objects:
	
	var cat1:Cat = ObjectFactory.fromContext("cont1").getInstance(Cat)
	var cat2:Cat = ObjectFactory.fromContext("cont2").getInstance(Cat)

Objects `cat1` and `cat2` are diffrent objects.

You can clear specific context (this will remove all objects from specific context) by execute:

	ObjectFactory.clear("cont1")

Or you can clear all contexts:

	ObjectFactory.clear()

It’s useful when you want create diffrent contexts per screen.

### Per call objects
If you add component to container, you can define that Injector will create new object for each call. After this initialization:

	Container.addComponent(Cat).forSameClass().withInstancePerCall()

all request for object will create a new object.

	var cat1:Cat = ObjectFactory.getInstance(Cat)
	var cat2:Cat = ObjectFactory.getInstance(Cat)

Objects `cat1` and `cat2` are diffrent objects.

## Installation
At the moment you have to copy Injector folder to your project. After holidays I will create cocoapods.